const needle = require('needle');

const endpoint = `http://api.geonames.org`;
const username = process.env.GEONAMES_USER;

class LocationService {

    static getLocationByCoordinate(gpsCoordinate) {
        return new Promise((resolve, reject) => {
            const path = `/findNearbyPlaceNameJSON?formatted=true&lat=${gpsCoordinate.lat}&lng=${gpsCoordinate.lon}&username=${username}&style=full`;

            console.log("Getting location from:" + path);

            needle.get(endpoint + path, (error, response) => {
                if (!error && response.statusCode === 200){
                    if(Array.isArray(response.body.geonames) && response.body.geonames.length > 0) {
                        resolve(response.body.geonames[0]);
                    } else {
                        reject("Location not found");
                    }
                } else {
                    reject(error);
                }

            });
        });
    }
}
exports.LocationService = LocationService;