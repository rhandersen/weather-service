const needle = require('needle');
const endpoint = `http://www.yr.no`;

class WeatherService {

    static getWeatherByLocation(location){
        return new Promise((resolve, reject) => {

            console.log("Getting weather from:" + WeatherService.getPathString(location));

            needle.get(endpoint + WeatherService.getPathString(location), (error, response) => {
                if (!error && response.statusCode === 200){
                    resolve(WeatherService.parseWeatherData(response.body));
                } else {
                    reject("An error happened while getting weather");
                }

            });
        });
    }

    static parseWeatherData(data){
        const out = {};
        const weatherdata = WeatherService.removeDollarsInTree(data.weatherdata);
        out.forecast = weatherdata.forecast.tabular.time[0];
        out.location = weatherdata.location;
        out.sun = weatherdata.sun;
        return out;
    }

    static removeDollarsInTree(data){
        if(typeof data === 'object') {
            for (let propertyName in data) {
                if (propertyName === '$') {
                    Object.assign(data, data[propertyName]);
                    delete data[propertyName];
                } else {
                    data[propertyName] = WeatherService.removeDollarsInTree(data[propertyName]);
                }
            }
            return data;
        } else {
            return data;
        }
    }

    static getPathString(location){
        return encodeURI(`/place/${location.countryName}/${location.adminName1}/${location.name}/forecast.xml`);
    }
}
exports.WeatherService = WeatherService;