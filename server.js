'use strict';

const express = require('express');
require('dotenv').config();

const {GpsCoordinate}  = require('./models/GpsCoordinate.js');
const {LocationService}  = require('./services/LocationService');
const {WeatherService}  = require('./services/WeatherService');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

app.get('/api/weather/:lat/:lon', (request, response) => {
    const coordinate = new GpsCoordinate(request.params.lat, request.params.lon);

    function handleError(error){
        response.statusCode = 500;
        response.send(error);
    }

    try {

        LocationService.getLocationByCoordinate(coordinate).then(location => {
            WeatherService.getWeatherByLocation(location).then(weather => {
                response.send(weather);
            }, handleError);
        }, handleError);
    } catch(error){
        handleError(error);
    }
});

app.listen(PORT, HOST);
console.log(`Running Weather Service on http://${HOST}:${PORT}`);