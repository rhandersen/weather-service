class GpsCoordinate {
    constructor(lat, lon){
        this.lat = lat;
        this.lon = lon;
    }
}
exports.GpsCoordinate = GpsCoordinate;